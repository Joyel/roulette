import java.util.Random;

public class RouletteWheel
{
    private Random randomizer;
    private int lastSpin;

    public RouletteWheel()
    {
        this.randomizer = new Random();
    }

    public void spin()
    {
        this.lastSpin = this.randomizer.nextInt(37);
    }

    public int getValue()
    {
        return this.lastSpin;
    }

    public boolean isLow()
    {
        if (this.lastSpin >=1 && this.lastSpin <= 18)
        {
            return true;
        }

        return false;
    }

    public boolean isHigh()
    {
        if (this.lastSpin >=19 && this.lastSpin <= 36)
        {
            return true;
        }

        return false;
    }

    public boolean isEven()
    {
        if (this.lastSpin != 0 && this.lastSpin % 2 == 0)
        {
            return true;
        }

        return false;
    }

    public boolean isOdd()
    {
        if (this.lastSpin % 2 == 1)
        {
            return true;
        }

        return false;
    }

    public boolean isRed()
    {
        if (this.lastSpin == 0)
        {
            return false;
        }
        
        if (((this.lastSpin >=1 && this.lastSpin <=10) || (this.lastSpin >=19 && this.lastSpin <=28)) && this.lastSpin %2 == 1)
        {
            return true;
        }

        if (((this.lastSpin >=11 && this.lastSpin <=18) || (this.lastSpin >=29 && this.lastSpin <=36)) && this.lastSpin %2 == 0)
        {
            return true;
        }

        return false;
    }

    /* Optional
    public boolean isBlack()
    {
        if (((this.lastSpin >=1 && this.lastSpin <=10) || (this.lastSpin >=19 && this.lastSpin <=28)) && this.lastSpin %2 == 0)
        {
            return true;
        }

        if (((this.lastSpin >=11 && this.lastSpin <=18) || (this.lastSpin >=29 && this.lastSpin <=36)) && this.lastSpin %2 == 1)
        {
            return true;
        }

        return false;
    }
    */
}