import java.util.Scanner;

public class Roulette
{
    public static void main(String[] args)
    {
        RouletteWheel spinWheel = new RouletteWheel();
        Scanner read = new Scanner(System.in);
        int playAgain = 1;
        int currentTotal = 0;

        System.out.println("Do you want to place a bet? (enter y/Y or n/N)");
        String betAnswer = read.next();

        if (betAnswer.equals("y") || betAnswer.equals("Y"))
        {
            while (playAgain == 1)
            {
                System.out.println("Enter 1 to bet on an exact number, 2 for odd/even, 3 for red/black.");
                int betType = read.nextInt();

                spinWheel.spin();

                if (betType == 1)
                {
                    System.out.println("What number do you want to bet on?");
                    int betNumber = read.nextInt();

                    System.out.println("How much do you want to bet on this?");
                    int betMoney = read.nextInt();
                
                    if (spinWheel.getValue() == betNumber)
                    {
                        currentTotal += (betNumber * betMoney);
                        System.out.println("You won $" + (betNumber * betMoney) + " this round!\nCurrent total: " + currentTotal);
                    }

                    else
                    {
                        currentTotal -= (betMoney);
                        System.out.println("You lost $" + (betMoney) + " this round...\nCurrent total: " + currentTotal);
                    }
                }

                if (betType == 2)
                {
                    System.out.println("Do you bet odd (1) or even (2)?");
                    int numberType = read.nextInt();

                    if (numberType == 1)
                    {
                        if (spinWheel.isOdd())
                        {
                            currentTotal += 1;
                            System.out.println("Odd number! You won $1 this round!\nCurrent total: " + currentTotal);
                        }

                        else
                        {
                            currentTotal -= 1;
                            System.out.println("Not an odd number. You lost $1 this round...\nCurrent total: " + currentTotal);
                        }
                    }

                    else
                    {
                        if (spinWheel.isEven())
                        {
                            currentTotal += 1;
                            System.out.println("Even number! You won $1 this round!\nCurrent total: " + currentTotal);
                        }

                        else
                        {
                            currentTotal -= 1;
                            System.out.println("Not an even number. You lost $1 this round...\nCurrent total: " + currentTotal);
                        }
                    }
                }

                if (betType == 3)
                {
                    System.out.println("Do you bet red (1) or black (2)?");
                    int colorType = read.nextInt();

                    if (colorType == 1)
                    {
                        if (spinWheel.isRed())
                        {
                            currentTotal += 1;
                            System.out.println("Red number! You won $1!");
                        }

                        else
                        {
                            currentTotal -= 1;
                            System.out.println("Not a red number. You lost $1...");
                        }
                    }

                    else
                    {
                        if (!(spinWheel.isRed()))
                        {
                            currentTotal += 1;
                            System.out.println("black number! You won $1!");
                        }

                        else
                        {
                            currentTotal -= 1;
                            System.out.println("Not a black number. You lost $1...");
                        }
                    }
                }

                System.out.println("\nPlay again? (enter y/Y or n/N)");
                String retry = read.next();

                if (retry.equals("n") || retry.equals("N"))
                {
                    playAgain = 0;
                }
            }

            if (currentTotal > 0)
            {
                System.out.println("\nYou won a total of $" + currentTotal + "! Congratulations!");
            }

            else if (currentTotal == 0)
            {
                System.out.println("\nYou didn't win any money. But at least you didn't lose any, either!\n");
            }

            else
            {
                System.out.println("You lost a total of $" + (currentTotal * -1) + ". Better luck next time...\n");
            }
        }

        else
        {
            System.out.println("Ok, bye.\n");
        }
        
    }
}